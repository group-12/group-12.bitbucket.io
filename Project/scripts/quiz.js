var question1 = document.getElementById("question1");
var question2 = document.getElementById("question2");
var question3 = document.getElementById("question3");
var question4 = document.getElementById("question4"); 

var acoustic = 0;
var electric = 0;
var metal = 0;

function musicChange(src) {
    question1.style.display = "none";
    question2.style.display = "block";

    switch (src.id) {
        case "music-folk":
            acoustic++;
            break;
        case "music-rock":
            electric++;
            break;
        case "music-metal":
            metal++;
            break;
        case "music-country":
            acoustic++;
            electric++;
            break;
        default:
            break;
    }
}

function expChange(src) {
    question2.style.display = "none";
    question3.style.display = "block";

    switch (src.id) {
        case "exp-none":
            acoustic++;
            break;
        case "exp-little":
            electric++;
            acoustic++;
            break;
        case "exp-med":
            electric++;
            metal++;
            break;
        case "exp-lots":
            electric++;
            metal++;
            break;
        default:
            break;
    }
}

function whyChange(src) {
    question3.style.display = "none";
    question4.style.display = "block";

    switch (src.id) {
        case "why-learn":
            acoustic++;
            break;
        case "why-cool":
            electric++;
            acoustic++;
            break;
        case "why-star":
            electric++;
            metal++;
            break;
        case "why-pro":
            electric++;
            metal++;
            break;
        default:
            break;
    }
}

function costChange(src) {
    question4.style.display = "none";

    switch (src.id) {
        case "cost-low":
            acoustic++;
            break;
        case "cost-med":
            electric++;
            break;
        case "cost-high":
            electric++;
            metal++;
            break;
        case "cost-noLimit":
            acoustic++;
            electric++;
            metal++;
            break;
        default:
            break;
    }
    showResult();
}

function showResult() {
    var result = document.getElementById("result");
    result.style.display = "block";

    var acousticCard = document.getElementById("acoustic");
    var electricCard = document.getElementById("electric");
    var metalCard = document.getElementById("metal");

    if ( acoustic >= electric && acoustic >= metal ) {
        acousticCard.style.display = "block";
    } else if ( electric >= acoustic && electric >= metal ) {
        electricCard.style.display = "block";
    } else if (metal >= acoustic && metal >= acoustic) {
        metalCard.style.display = "block";
    } else {
        acousticCard.style.display = "block";
        electricCard.style.display = "block";
        metalCard.style.display = "block";
    }
}