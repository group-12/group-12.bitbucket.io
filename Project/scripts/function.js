// Function to check an email is valid
function checkEmail() {
    var email = document.getElementById('email');
    var validText = document.getElementById('validEmailLabel')
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if (!filter.test(email.value)) {
        email.style.color = "#ff0000";
        validText.hidden = false;
        return false;
    }
    email.style.color = "#000000";
    validText.hidden = true;
    return true;
}

function checkName() {
    var name = document.getElementById('name');
    var validNameText = document.getElementById('validNameLabel')
    var letters = /^[a-zA-Z]+( [a-zA-Z]+)+$/;

    if (!name.value.match(letters) && !name.value == "") {
        name.style.color = "#ff0000";
        validNameText.hidden = false;
        return false;
    } else {
        name.style.color = "#000000";
        validNameText.hidden = true;
    }

    return true;
}

// Search bar functions
// Hide and show the dropdown list
function dropDown() {
    document.getElementById("searchDropdown").classList.toggle("show");
}

// source: https://www.w3schools.com/howto/howto_js_filter_dropdown.asp
function searchSite() {
    var i
    var input = document.getElementById("searchbar");
    var filter = input.value.toUpperCase();
    div = document.getElementById("searchDropdown");
    var a = div.getElementsByTagName("a");
    for (i = 0; i < a.length; i++) {
        txtValue = a[i].textContent || a[i].innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            a[i].style.display = "";
        } else {
            a[i].style.display = "none";
        }
    }
}

// lets user know when the page is loaded via the console (source: https://www.youtube.com/watch?v=YeFzkC2awTM)
window.addEventListener('load', function () {
    var page = "_";
    page = document.getElementById("currentpage").innerHTML
    console.log('The %o page has now fully loaded', page)
});

// Mobile friendly nav menu (source: https://www.w3schools.com/howto/tryit.asp?filename=tryhow_js_mobile_navbar)
function burgerMenu() {
    var x = document.getElementById("menuLinks");
    if (x.style.display === "block") {
        x.style.display = "none";
    } else {
        x.style.display = "block";
    }
}

// star clicked
function starClick(starId) {
    var stars = document.getElementsByClassName("star")
    var len = stars.length
    for (var i = 0; i < len; ++i) {
        if (stars[i].id > starId) {
            stars[i].className = "star far fa-star"
        } else {
            stars[i].className = "star fas fa-star"
        }
    }
    var link = document.getElementById("link")
    if (link.hidden) {
        document.getElementById("text").innerText = `Thanks for rating us ${starId}/${len}!`
    } else {
        document.getElementById("text").innerText = `Thanks for updating your rating ${starId}/${len}!`
    }
    if (starId == len) {
        link.innerText = "Tell us why do you think we deserve perfect rating!"
    } else {
        link.innerText = "Tell us how we may improve!"
    }
    link.hidden = false
}