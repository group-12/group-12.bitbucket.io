// Checks to see if page is done loading (source:https://www.youtube.com/watch?v=YeFzkC2awTM)
if (document.readyState == 'loading') {
    document.addEventListener('DOMContentLoaded', ready)
} else {
    ready()
}

// Event Listeners and associated functions (source: https://www.youtube.com/watch?v=YeFzkC2awTM)
function ready() {
    var removeCartItemButtons = document.getElementsByClassName('btn_remove')
    for (var i = 0; i < removeCartItemButtons.length; i++) {
        var button = removeCartItemButtons[i]
        button.addEventListener('click', removeCartItem)
    }

    var quantityInputs = document.getElementsByClassName('cart_quantity_input')
    for (var i = 0; i < quantityInputs.length; i++) {
        var input = quantityInputs[i]
        input.addEventListener('change', quantityChanged)
    }

    var addToCartButtons = document.getElementsByClassName('btn-add')
    for (var i = 0; i < addToCartButtons.length; i++) {
        var button = addToCartButtons[i]
        button.addEventListener('click', addToCartClicked)
    }

    document.getElementsByClassName('btn-payment')[0].addEventListener('click', paymentClicked)
    loadCart()
    window.onbeforeunload = storeCart
}

// Removes products from cart (source: https://www.youtube.com/watch?v=YeFzkC2awTM)
function paymentClicked() {
    alert('Payment successful')
    var cartItems = document.getElementsByClassName('cart_items')[0]
    while (cartItems.hasChildNodes()) {
        cartItems.removeChild(cartItems.firstChild)
    }
    updateCartTotal()
}

// Removes items from cart (source: https://www.youtube.com/watch?v=YeFzkC2awTM)
function removeCartItem(event) {
    var buttonClicked = event.target
    buttonClicked.parentElement.parentElement.remove()
    updateCartTotal()
}

// Updates the total price with respect to the quantity (source: https://www.youtube.com/watch?v=YeFzkC2awTM)
function quantityChanged(event) {
    var input = event.target
    if (isNaN(input.value) || input.value <= 0) {
        input.value = 1
    }
    updateCartTotal()
}

// Stores products title and price (source: https://www.youtube.com/watch?v=YeFzkC2awTM)
function addToCartClicked(event) {
    var button = event.target
    var shopItem = button.parentElement.parentElement
    var title = shopItem.getElementsByClassName('card-title')[0].innerText
    var price = shopItem.getElementsByClassName('product-price')[0].innerText
    addItemToCart(title, price)
    updateCartTotal()
}

// Add's product to cart (source: https://www.youtube.com/watch?v=YeFzkC2awTM)
function addItemToCart(title, price) {
    var cartRow = document.createElement('div')
    cartRow.classList.add('cart_row')
    var cartItems = document.getElementsByClassName('cart_items')[0]
    var cartItemNames = cartItems.getElementsByClassName('cart_current_product')
    for (var i = 0; i < cartItemNames.length; i++) {
        if (cartItemNames[i].innerText == title) {
            alert('Already added this product to the cart')
            return
        }
    }
    var cartRowContents = `
    <div class="cart_product cart-basic cart_column">
       <p class="cart_current_product">${title}</p>     
    </div>
    <div class="cart_quantity cart-basic cart_column">
        <input class="cart_quantity_input" type="number" value="1">
        <button class="btn_remove" type="button">Remove</button>
    </div>
    <p class="cart_price cart-basic cart_column">${price}</p>`
    cartRow.innerHTML = cartRowContents
    cartItems.append(cartRow)
    cartRow.getElementsByClassName('btn_remove')[0].addEventListener('click', removeCartItem)
    cartRow.getElementsByClassName('cart_quantity_input')[0].addEventListener('change', quantityChanged)
}

// Updates total price for cart (source: https://www.youtube.com/watch?v=YeFzkC2awTM)
function updateCartTotal() {
    var cartItemContainer = document.getElementsByClassName('cart_items')[0]
    var cartRows = cartItemContainer.getElementsByClassName('cart_row')
    var total = 0
    for (var i = 0; i < cartRows.length; i++) {
        var cartRow = cartRows[i]
        var priceElement = cartRow.getElementsByClassName('cart_price')[0]
        var quantityElement = cartRow.getElementsByClassName('cart_quantity_input')[0]
        var price = parseFloat(priceElement.innerText.replace('$', ''))
        var quantity = quantityElement.value
        total = total + (price * quantity)
    }
    total = Math.round(total * 100) / 100
    document.getElementsByClassName('cart_total_price')[0].innerText = '$' + total
}

// store cart data in sessionstorage
function storeCart() {
    var cartItems = []
    var cartItemContainer = document.getElementsByClassName('cart_items')[0]
    var cartRows = cartItemContainer.getElementsByClassName('cart_row')
    for (var i = 0; i < cartRows.length; i++) {
        var cartRow = cartRows[i]
        var itemTitle = cartRow.getElementsByClassName("cart_current_product")[0].innerHTML
        var itemPrice = cartRow.getElementsByClassName("cart_price")[0].innerHTML
        var itemQuantity = cartRow.getElementsByClassName("cart_quantity_input")[0].value
        cartItems.push([itemTitle, itemPrice, itemQuantity])
    }
    sessionStorage.setItem("cartItems", JSON.stringify(cartItems))
}

// load cart data from sessionstorage
function loadCart() {
    var cartItems = JSON.parse(sessionStorage.getItem('cartItems'))
    if (cartItems) {
        var cartItemsHTML = document.getElementsByClassName('cart_items')[0]
        for (var i = 0; i < cartItems.length; i++) {
            var cartItem = cartItems[i]
            addItemToCart(cartItem[0], cartItem[1])
            cartItemsHTML.getElementsByClassName("cart_quantity_input")[i].value = cartItem[2]
        }
        updateCartTotal()
    }
}