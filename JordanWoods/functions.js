// Jordan Woods
// S315838
// Group 12

// Check all items are filled out correctly before submitting form
function checkAll() {
    if (checkName() == false) {
        alert("Please enter a valid first and last name");
        return false;
    } else if (checkEmail() == false) {
        alert("Please enter a valid email address");
        return false;
    } else if (checkPassword() == false) {
        alert("Please enter a password");
        return false;
    } else if (checkAge() == false) {
        alert("Please enter a valid birthdate");
        return false;
    } else if (checkPhone() == false) {
        alert("Please enter a valid mobile number");
        return false;
    } else {
        // Submit form if all filled in correctly
        document.getElementById('registrationForm').submit();
        return true;
    }
}

// Function to check an email is valid
function checkEmail() {
    var email = document.getElementById('email');
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if (!filter.test(email.value)) {
        email.style.color = "#ff0000";
        return false;
    }
    email.style.color = "#000000";
    return true;
}

// Function to check the name has no invalid characters
function checkName() {
    var firstname = document.getElementById('fname');
    var lastname = document.getElementById('lname');
    var letters = /^[A-Za-z]+$/;

    if (!firstname.value.match(letters) && !firstname.value == "") {
        firstname.style.color = "#ff0000";
        return false;
    } else {
        firstname.style.color = "#000000";
    }
    if (!lastname.value.match(letters) && !lastname.value == "") {
        lastname.style.color = "#ff0000";
        return false;
    } else {
        lastname.style.color = "#000000";
    }

    // Check to ensure they are not blank
    if (firstname == "" || lastname == "") {
        return false;
    }
    return true;
}

// Check age from birthdate given
function checkAge() {
    var birth = document.getElementById('birthdate').value;
    var today = new Date();
    var birthDate = new Date(birth);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    document.getElementById('age').value = age;

    if (age < 13) {
        alert("You must be over 13 to use this registration form");
        return false;
    } else if (age > 50) {
        document.getElementById('overfifty').style.display = 'block';
    }
}

// Check phone number has 10 digets 
function checkPhone() {
    var number = document.getElementById('phnumber');
    var validphone = /^\d{10}$/;

    // Check if blank
    if (number == "") {
        return false;
    }

    if (number.value.match(validphone)) {
        number.style.color = "#000000";
        return true;
    } else {
        number.style.color = "#ff0000";
        return false;
    }
}

// Function to check passwords match
function checkPassword() {
    var pass = document.getElementById('password');
    var check = document.getElementById('password_re');

    if (pass.value == "" || check.value == "") {
        return false;
    }

    if (pass.value != check.value) {
        alert("Passwords do not match");
        check.style.color = "#ff0000";
        pass.value = "";
        check.value = "";
        return false;
    }
    check.style.color = "#000000";
    return true;
}